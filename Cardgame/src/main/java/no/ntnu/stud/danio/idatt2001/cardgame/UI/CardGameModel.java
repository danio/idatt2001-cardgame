package no.ntnu.stud.danio.idatt2001.cardgame.UI;

import no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.DeckOfCards;

/**
 * Not sure if this even does anything...
 */
public class CardGameModel {
    private final DeckOfCards deck;

    protected CardGameModel() {
        this.deck = new DeckOfCards();
    }

    public DeckOfCards getDeck() {
        return deck;
    }
}
