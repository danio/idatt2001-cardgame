package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import java.util.Arrays;
import java.util.stream.Stream;

// In any other language I'd use a tuple struct :)

/**
 * All playing cards in a standard 52-deck.
 * By using enums I get natural ordering for free
 * It should be easier for the compiler to statically infer things about the program
 * It will guarantee there's no objects being duplicated around the code
 * I wanted to try using them
 */
public enum PlayingCard {
    ACE_CLUBS (CardFace.ACE, CardSuit.CLUBS),
    TWO_CLUBS (CardFace.TWO, CardSuit.CLUBS),
    THREE_CLUBS (CardFace.THREE, CardSuit.CLUBS),
    FOUR_CLUBS (CardFace.FOUR, CardSuit.CLUBS),
    FIVE_CLUBS (CardFace.FIVE, CardSuit.CLUBS),
    SIX_CLUBS (CardFace.SIX, CardSuit.CLUBS),
    SEVEN_CLUBS (CardFace.SEVEN, CardSuit.CLUBS),
    EIGHT_CLUBS (CardFace.EIGHT, CardSuit.CLUBS),
    NINE_CLUBS (CardFace.NINE, CardSuit.CLUBS),
    TEN_CLUBS (CardFace.TEN, CardSuit.CLUBS),
    JACK_CLUBS (CardFace.JACK, CardSuit.CLUBS),
    QUEEN_CLUBS (CardFace.QUEEN, CardSuit.CLUBS),
    KING_CLUBS (CardFace.KING, CardSuit.CLUBS),

    ACE_DIAMONDS (CardFace.ACE, CardSuit.DIAMONDS),
    TWO_DIAMONDS (CardFace.TWO, CardSuit.DIAMONDS),
    THREE_DIAMONDS (CardFace.THREE, CardSuit.DIAMONDS),
    FOUR_DIAMONDS (CardFace.FOUR, CardSuit.DIAMONDS),
    FIVE_DIAMONDS (CardFace.FIVE, CardSuit.DIAMONDS),
    SIX_DIAMONDS (CardFace.SIX, CardSuit.DIAMONDS),
    SEVEN_DIAMONDS (CardFace.SEVEN, CardSuit.DIAMONDS),
    EIGHT_DIAMONDS (CardFace.EIGHT, CardSuit.DIAMONDS),
    NINE_DIAMONDS (CardFace.NINE, CardSuit.DIAMONDS),
    TEN_DIAMONDS (CardFace.TEN, CardSuit.DIAMONDS),
    JACK_DIAMONDS (CardFace.JACK, CardSuit.DIAMONDS),
    QUEEN_DIAMONDS (CardFace.QUEEN, CardSuit.DIAMONDS),
    KING_DIAMONDS (CardFace.KING, CardSuit.DIAMONDS),

    ACE_HEARTS (CardFace.ACE, CardSuit.HEARTS),
    TWO_HEARTS (CardFace.TWO, CardSuit.HEARTS),
    THREE_HEARTS (CardFace.THREE, CardSuit.HEARTS),
    FOUR_HEARTS (CardFace.FOUR, CardSuit.HEARTS),
    FIVE_HEARTS (CardFace.FIVE, CardSuit.HEARTS),
    SIX_HEARTS (CardFace.SIX, CardSuit.HEARTS),
    SEVEN_HEARTS (CardFace.SEVEN, CardSuit.HEARTS),
    EIGHT_HEARTS (CardFace.EIGHT, CardSuit.HEARTS),
    NINE_HEARTS (CardFace.NINE, CardSuit.HEARTS),
    TEN_HEARTS (CardFace.TEN, CardSuit.HEARTS),
    JACK_HEARTS (CardFace.JACK, CardSuit.HEARTS),
    QUEEN_HEARTS (CardFace.QUEEN, CardSuit.HEARTS),
    KING_HEARTS (CardFace.KING, CardSuit.HEARTS),

    ACE_SPADES (CardFace.ACE, CardSuit.SPADES),
    TWO_SPADES (CardFace.TWO, CardSuit.SPADES),
    THREE_SPADES (CardFace.THREE, CardSuit.SPADES),
    FOUR_SPADES (CardFace.FOUR, CardSuit.SPADES),
    FIVE_SPADES (CardFace.FIVE, CardSuit.SPADES),
    SIX_SPADES (CardFace.SIX, CardSuit.SPADES),
    SEVEN_SPADES (CardFace.SEVEN, CardSuit.SPADES),
    EIGHT_SPADES (CardFace.EIGHT, CardSuit.SPADES),
    NINE_SPADES (CardFace.NINE, CardSuit.SPADES),
    TEN_SPADES (CardFace.TEN, CardSuit.SPADES),
    JACK_SPADES (CardFace.JACK, CardSuit.SPADES),
    QUEEN_SPADES (CardFace.QUEEN, CardSuit.SPADES),
    KING_SPADES (CardFace.KING, CardSuit.SPADES);


    private final CardFace face;
    private final CardSuit suit;

    PlayingCard(CardFace face, CardSuit suit) {
        this.face = face;
        this.suit = suit;
    }

    public CardFace getFace() {
        return face;
    }

    public CardSuit getSuit() {
        return suit;
    }

    /**
     * @param face number corresponding to the face
     * @param suit short letter corresponding to the suit
     * @return corresponding playing card to the face and suit
     * @throws IllegalArgumentException if the face or suit do not represent a legal card
     */
    public static PlayingCard selectPlayingCard(int face, char suit) throws IllegalArgumentException {
        var card = Arrays.stream(PlayingCard.values())
                .filter(pc -> pc.getFace() == CardFace.selectCardFace(face))
                .filter(pc -> pc.getSuit()== CardSuit.selectSuit(suit))
                .findFirst();
        return card.orElseThrow(() -> new IllegalArgumentException(face + ", " + suit + ": Not a legal card selection"));
    }

    public static Stream<PlayingCard> stream() {
        return Stream.of(values());
    }

    /**
     * Returns the suit and face of the card as a string.
     * A 4 of hearts is returned as the string "H4".
     *
     * @return the suit and face of the card as a string
     */
    public String getAsString() {
        return String.format("%s%s", suit.getShort(), face.getNumber());
    }
}