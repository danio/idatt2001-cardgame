package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * a hand of cards, dereferences somewhat to an ArrayList.
 */
public class CardHand implements Iterable<PlayingCard> {
    /**
     * Array list because switching places in hands seem like a reasonable thing to do
     */
    private ArrayList<PlayingCard> hand = new ArrayList<PlayingCard>();

    public CardHand() {}

    public CardHand(ArrayList<PlayingCard> hand) {
        this.hand = hand;
    }

    /**
     *
     * @return the underlying list of cards
     */
    public List<PlayingCard> getHand() {
        return hand;
    }

    /**
     * Add a card ot the hand
     * @param c the card to add to the hand
     * @return true if success false if not (same as other Collections)
     */
    public boolean add(PlayingCard c) {
        return hand.add(c);
    }

    /**
     * add many cards to the hand
     * @param c collection of cards to add to the hand
     * @return success, same as other Collections
     */
    public boolean addAll(Collection<PlayingCard> c) {
        return hand.addAll(c);
    }

    /**
     * @return a stream of cards in the hand
     */
    public Stream<PlayingCard> stream() {
        return hand.stream();
    }

    @Override
    public Iterator<PlayingCard> iterator() {
        return hand.iterator();
    }

    /**
     *
     * @return space separated list of cards in the hand in shirt form
     */
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        for (var card : this) {
            builder.append(card.getAsString());
            builder.append(" ");
        }
        return builder.toString().trim();
    }

    /**
     * @return sum of cards in the hand
     */
    public int sum() {
        return this.stream().map(c -> c.getFace().getNumber()).reduce(0, Integer::sum);
    }

    /**
     * check if hand contains an n-flush
     * @param n how many cards need to be of the same suit
     * @return true if contained a flush
     */
    public boolean flush(int n) {
        int clubs = 0;
        int diamonds = 0;
        int hearts = 0;
        int spades = 0;
        for (var card : hand) {
            switch (card.getSuit()) {
                case CLUBS:
                    clubs++;
                    break;
                case DIAMONDS:
                    diamonds++;
                    break;
                case HEARTS:
                    hearts++;
                    break;
                case SPADES:
                    spades++;
                    break;
            }
        }
        return clubs >= n || diamonds >= n || hearts >= n || spades >= n;
    }
}
