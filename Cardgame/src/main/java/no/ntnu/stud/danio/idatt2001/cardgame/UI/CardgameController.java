package no.ntnu.stud.danio.idatt2001.cardgame.UI;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.CardHand;
import no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.CardSuit;
import no.ntnu.stud.danio.idatt2001.cardgame.CardgameApplication;
import no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.PlayingCard;

import java.io.IOException;

public class CardgameController {

    CardGameModel model;
    CardHand currentHand;

    @FXML
    HBox displayedHand;

    @FXML
    Text resultHand;

    public CardgameController() {
        this.model = new CardGameModel();
        this.currentHand = new CardHand();
    }

    @FXML
    protected void onDealClick() throws IOException {
        model.getDeck().shuffle();
        var hand = model.getDeck().dealHand(5);
        this.currentHand = hand;

        displayedHand.getChildren().removeIf(x -> true); // Delete all cards in the view
        for (PlayingCard card : hand) {
            displayedHand.getChildren().add(makeCard(card)); //add child nodes for each card the HBox
        }
    }

    @FXML
    protected void onCheckClick() {
        StringBuilder builder = new StringBuilder();
        builder.append("The sum of all the cards is " + currentHand.sum());

        String hearts = currentHand.stream()
                .filter(c -> c.getSuit() == CardSuit.HEARTS)
                .map(PlayingCard::getAsString)
                .reduce("", (a, b) -> String.format("%s %s", a, b))
                .trim();
        builder.append("\t Hearts: " + hearts);

        boolean queenOfSpades = currentHand.stream().anyMatch(c -> c == PlayingCard.QUEEN_SPADES);
        builder.append("\nQueen of spades? " + queenOfSpades);

        boolean fiveFlush = currentHand.flush(5);
        builder.append("\nFive-flush: " + fiveFlush);

        resultHand.setText(builder.toString());
    }

    /**
     *
     * @param card which card to render
     * @return a BorderPane which is actually a whole playing card in its child nodes
     * @throws IOException
     */
    private static BorderPane makeCard(PlayingCard card) throws IOException {
        var loader = new FXMLLoader(CardgameController.class.getResource("card-view.fxml"));
        BorderPane node = loader.load();
        CardController controller = loader.getController();

        controller.setCard(card);

        return node;
    }

}
