package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * A deck of cards
 * can run out!
 */
public class DeckOfCards {

    private final List<PlayingCard> deck;

    /**
     * Construct a deck full of cards
     */
    public DeckOfCards() {
        var deck = new ArrayList<PlayingCard>(52);
        Collections.addAll(deck, PlayingCard.values());
        this.deck = deck;
    }

    /**
     * Construct a new deck from a list of cards
     * @param deck a list of cards to access as a deck.
     */
    public DeckOfCards(List<PlayingCard> deck) {
        this.deck = deck;
    }

    /**
     * Shuffle the deck
     * You should probably do this before dealing
     */
    public void shuffle() {
        Collections.shuffle(deck);
    }

    /**
     * take cards from the deck and put them in some collection.
     * This is mostly here because I wanted to do a cool thing with generics.
     * Could have gotten the same thing pretty much by just using dependency injection and adding to a Collection.
     * @param n how many cards to take from the deck
     * @param collectionFactory What kind of collection do you want them in?
     * @param <C> Some Collection type
     * @return a new Collection of the type specified
     */
    public <C extends Collection<PlayingCard>> C takeCards(int n, Supplier<C> collectionFactory) {
        var hand =  deck.stream().limit(n).collect(Collectors.toCollection(collectionFactory));
        deck.removeAll(hand);
        return hand;
    }

    /**
     * take cards from the deck
     * @param n how many cards to take
     * @return a hashset of cards taken from the deck
     */
    public HashSet<PlayingCard> takeCards(int n) {
        return takeCards(n, HashSet::new);
    }

    /**
     * same as takeCards but gives you them as a new hand
     * @param n how many cards ot take
     * @return hand of cards
     */
    public CardHand dealHand(int n) {
        var hand = new CardHand();
        hand.addAll(takeCards(n));
        return hand;
    }

}

