package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import java.util.Arrays;

/**
 * enum of all possible cardfaces (1-13)
 */
public enum CardFace {
    ACE (1),
    TWO (2),
    THREE (3),
    FOUR (4),
    FIVE (5),
    SIX (6),
    SEVEN (7),
    EIGHT (8),
    NINE (9),
    TEN (10),
    JACK (11),
    QUEEN (12),
    KING (13);

    private final int face;

    CardFace(int face) {
        this.face = face;
    }

    /**
     * @return numerical representation of cardface
     */
    public int getNumber() {
        return face;
    }

    /**
     * a static function to convert a number to the corresponding cardFace
     * @param number between 1 and 13
     * @return Corresponding CardFace for provided number
     * @throws IllegalArgumentException if there's no card that fits this number
     */
    public static CardFace selectCardFace(int number) throws IllegalArgumentException {
        var card = Arrays.stream(CardFace.values())
                .filter(cf -> cf.getNumber() == number).findFirst();
        return card.orElseThrow(() -> new IllegalArgumentException(number + " is not a valid card face"));
    }
}
