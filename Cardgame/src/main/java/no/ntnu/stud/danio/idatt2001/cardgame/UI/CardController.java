package no.ntnu.stud.danio.idatt2001.cardgame.UI;

import javafx.fxml.FXML;
import javafx.scene.text.Text;
import no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.PlayingCard;

public class CardController {
    @FXML
    private Text topCorner;

    @FXML
    private Text middle;

    @FXML
    private Text bottomCorner;

    public CardController() {}

    /**
     * Sets the numbers and symbols on the card.
     * @param card What should the card look like
     */
    public void setCard(PlayingCard card) {
        var suit = card.getSuit().getPretty();
        var cornerText = String.format("%s\n%s", card.getFace().getNumber(), suit);
        topCorner.setText(cornerText);
        middle.setText(card.getSuit().getPretty());
        bottomCorner.setText(cornerText);
    }
}
