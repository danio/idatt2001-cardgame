package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import java.util.Arrays;

/**
 * enum of all possible suits of cards
 */
public enum CardSuit {
    SPADES ('S'),
    HEARTS ('H'),
    DIAMONDS ('D'),
    CLUBS ('C');

    private final char shortSuit;

    CardSuit(char shortSuit) {
        this.shortSuit = shortSuit;
    }

    /**
     * @return short representation of suit
     */
    public char getShort() {
        return shortSuit;
    }

    /**
     * @return one-letter string containing the emoji corresponding to the suit
     */
    public String getPretty() {
        switch (this) {
            case SPADES:
                return "♠";
            case HEARTS:
                return "♥";
            case DIAMONDS:
                return "♦";
            case CLUBS:
                return "♣";
        }
        return null;
    }

    /**
     *
     * @param shortSuit First letter of the suit (C - Clubs, S - Spades, D - Diamonds, H - Hearts)
     * @return corresponding CardSuit for the short letter
     * @throws IllegalArgumentException if letter doesn't fit any suit
     */
    public static CardSuit selectSuit(char shortSuit) throws IllegalArgumentException {
        var card = Arrays.stream(CardSuit.values())
                .filter(cf -> cf.getShort() == shortSuit).findFirst();
        return card.orElseThrow(() -> new IllegalArgumentException(shortSuit + " is not a valid card suit"));
    }
}
