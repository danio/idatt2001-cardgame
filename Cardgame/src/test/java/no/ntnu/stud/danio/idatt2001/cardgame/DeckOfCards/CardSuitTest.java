package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardSuitTest {

    @Test
    void getShort() {
        assertEquals('C', CardSuit.CLUBS.getShort());
        assertEquals('D',CardSuit.DIAMONDS.getShort());
    }

    @Test
    void selectSuit() {
        assertEquals(CardSuit.selectSuit('S'), CardSuit.SPADES);
        assertThrows(IllegalArgumentException.class, () -> CardSuit.selectSuit('Q'));
    }
}