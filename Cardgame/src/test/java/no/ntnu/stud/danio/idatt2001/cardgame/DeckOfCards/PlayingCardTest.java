package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayingCardTest {

    @Test
    void selectPlayingCard() {
        assertEquals(PlayingCard.selectPlayingCard(1, 'S'), PlayingCard.ACE_SPADES);
        assertEquals(PlayingCard.selectPlayingCard(11, 'D'), PlayingCard.JACK_DIAMONDS);

        assertThrows(IllegalArgumentException.class, () -> PlayingCard.selectPlayingCard(23, 'H'));
        assertThrows(IllegalArgumentException.class, () -> PlayingCard.selectPlayingCard(10, 'Q'));
        assertThrows(IllegalArgumentException.class, () -> PlayingCard.selectPlayingCard(23, 'L'));
    }

    @Test
    void getAsString() {
        assertEquals(PlayingCard.ACE_SPADES.getAsString(), "S1");
        assertEquals(PlayingCard.KING_SPADES.getAsString(), "S13");
    }
}