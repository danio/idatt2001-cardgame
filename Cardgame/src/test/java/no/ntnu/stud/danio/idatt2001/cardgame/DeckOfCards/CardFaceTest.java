package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardFaceTest {

    @Test
    void getFace() {
        var cardFace = CardFace.FOUR;
        assertEquals(cardFace.getNumber(), 4);
        assertEquals(CardFace.KING.getNumber(), 13);
    }

    @Test
    void selectCardFace() {
        assertEquals(CardFace.selectCardFace(1), CardFace.ACE);
        assertEquals(CardFace.selectCardFace(11), CardFace.JACK);

        assertThrows(IllegalArgumentException.class, () -> CardFace.selectCardFace(99));
    }
}