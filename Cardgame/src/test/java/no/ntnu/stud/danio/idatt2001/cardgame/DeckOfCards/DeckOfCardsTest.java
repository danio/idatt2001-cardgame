package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.PlayingCard.*;
import static org.junit.jupiter.api.Assertions.*;

class DeckOfCardsTest {

    @Test
    void takeCards() {
        var deck = new DeckOfCards();
        var hand = deck.takeCards(1, ArrayList::new);
        assertEquals(ArrayList.class, hand.getClass());
    }

    @Test
    void simpleTakeCards() {
        var deck = new DeckOfCards();
        var hand = deck.takeCards(1);
        assertTrue(hand.contains(ACE_CLUBS));
        assertFalse(hand.contains(TWO_CLUBS));
        assertFalse(hand.contains(ACE_SPADES));

        hand = deck.takeCards(5);
        var expected = new PlayingCard[] {TWO_CLUBS, THREE_CLUBS, FOUR_CLUBS, FIVE_CLUBS, SIX_CLUBS};
        assertTrue(hand.containsAll(List.of(expected)));
        assertEquals(5, hand.size());

        deck.takeCards(52-6-1);

        hand = deck.takeCards(10);
        assertEquals(1, hand.size());
    }

    @Test
    void dealHand() {
        var deck = new DeckOfCards();
        var hand = deck.dealHand(5);
        assertEquals(hand.getClass(), CardHand.class);

        hand.getHand().sort(null);
        assertEquals("C1 C2 C3 C4 C5", hand.toString().trim());
    }
}