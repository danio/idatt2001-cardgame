package no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static no.ntnu.stud.danio.idatt2001.cardgame.DeckOfCards.PlayingCard.*;
import static org.junit.jupiter.api.Assertions.*;

class CardHandTest {

    @Test
    void testToString() {
        var hand = new CardHand(new ArrayList<PlayingCard>(List.of(new PlayingCard[]{ACE_SPADES, KING_HEARTS})));
        assertEquals("S1 H13", hand.toString().trim());
    }

    @Test
    void sum() {
        var hand = new CardHand(new ArrayList<PlayingCard>(List.of(new PlayingCard[]{ACE_SPADES, KING_HEARTS})));
        assertEquals(14, hand.sum());
        assertNotEquals(21, hand.sum());
    }

    @Test
    void flush() {
        var hand = new CardHand(new ArrayList<PlayingCard>(List.of(new PlayingCard[]{
                ACE_SPADES, KING_SPADES}
        )));
        assertTrue(hand.flush(2));
        assertFalse(hand.flush(3));

        hand = new CardHand(new ArrayList<PlayingCard>(List.of(new PlayingCard[]{
                ACE_SPADES, KING_HEARTS, THREE_HEARTS}
        )));

        assertTrue(hand.flush(2));
        assertFalse(hand.flush(3));
    }
}