# Building
Har bare testet å bygge dette med en SDK som allerede inneholder moderne versjoner av javafx (se https://wiki.openjdk.java.net/display/OpenJFX/Building+OpenJFX#BuildingOpenJFX-IntegrationwithOpenJDK) (du kan bruke nix-shellet for å få tilgang til den SDKen).

På andre OS tror jeg kanskje azul zulu sine openjdk builds er en option?

Om buildet ikke funker så er det sikkert bare å legge til noen biblioteker i pom-fila.
